.PHONY: install-argocd check-ready

install-argo:
	kubectl create ns argocd
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

check-ready:
	kubectl wait --for=condition=available deployment -l "app.kubernetes.io/name=argocd-server" -n argocd --timeout=300s

start-bootstrap:
	kubectl apply -f argo-bootstrap/bootstrap.yaml