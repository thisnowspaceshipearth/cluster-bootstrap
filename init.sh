#! /bin/zsh

# start minikube
minikube start

# make
make install-argo
make check-ready
make start-bootstrap